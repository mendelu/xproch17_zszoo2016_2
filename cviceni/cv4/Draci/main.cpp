#include <iostream>
using namespace std;

class Zviratko{
private:
    int m_vaha;
    int m_kalorie;
public:
    Zviratko(int vaha, int kalorie){
        m_vaha = vaha;
        m_kalorie = kalorie;
    }

    int getVaha(){
        return m_vaha;
    }

    int getKalorie(){
        return m_kalorie;
    }
};

class Princ{
private:
    int m_vaha;
    int m_kalorie;
    string m_jmeno;
public:
    Princ(int vaha, int kalorie,string jmeno){
        m_vaha = vaha;
        m_kalorie = kalorie;
        m_jmeno = jmeno;
    }

    int getVaha(){
        return m_vaha;
    }

    int getKalorie(){
        return m_kalorie;
    }

    string getJmeno(){
        return m_jmeno;
    }
};

class Drak{
private:
    int m_sezraneKalorie;
    int m_sezranaVaha;

public:
    Drak(){
        m_sezraneKalorie = 0;
        m_sezranaVaha = 0;
    }

    Drak(int pocatecniKalorie, int pocatecniVaha){
        m_sezraneKalorie = pocatecniKalorie;
        m_sezranaVaha = pocatecniVaha;
    }

    void sezer(int sezraneKalorie, int sezranaVaha){
        m_sezraneKalorie += sezraneKalorie;
        m_sezranaVaha += sezranaVaha;
        //m_sezranaVaha = m_sezranaVaha + sezranaVaha;
    }

    void sezer(Princ* zranyPrinc){
        m_sezraneKalorie += zranyPrinc->getKalorie();
        m_sezranaVaha += zranyPrinc->getVaha();
    }

    void printInfo(){
        cout << "Vaha: " << m_sezranaVaha << endl;
        cout << "Kalorie: " << m_sezraneKalorie << endl;
    }
};

int main()
{
    Zviratko* veverka = new Zviratko(10, 1);
    Princ* krason = new Princ(10000, 90, "Karel Krason");
    Drak* smak = new Drak();

    smak->sezer(veverka->getKalorie(), veverka->getVaha());
    smak->printInfo();

    smak->sezer(krason);
    smak->printInfo();

    delete smak;
    delete krason;
    delete veverka;
    return 0;
}
