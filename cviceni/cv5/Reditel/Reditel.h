#ifndef REDITEL_H
#define REDITEL_H

#include <iostream>
#include "Zamestnanec.h"
using namespace std;

class Reditel {

private:
    string m_jmeno;
	Zamestnanec* m_asistent;

public:
	Reditel(string jmeno);
	string getJmeno();
	string getJmenoAsistenta();
	void zmenAsistenta(Zamestnanec* novyAsistent);
	void zvysPlatAsistenta(int zvyseni);
};

#endif // REDITEL_H
