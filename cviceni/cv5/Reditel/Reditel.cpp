#include "Reditel.h"

Reditel::Reditel(string jmeno) {
	m_jmeno = jmeno;
	m_asistent = nullptr;
}

string Reditel::getJmeno() {
	return m_jmeno;
}

string Reditel::getJmenoAsistenta() {
    if (m_asistent != nullptr){
        return m_asistent->getJmeno();
    } else {
        return "";
    }
}

void Reditel::zmenAsistenta(Zamestnanec* novyAsistent) {
	m_asistent = novyAsistent;
}

void Reditel::zvysPlatAsistenta(int zvyseni){
    if (m_asistent != nullptr){
        m_asistent->zvysPlat(zvyseni);
    } else {
        cout << "Nemas asistenta, nemuzes zvedat plat" << endl;
    }
}
