#include <iostream>
#include "Zamestnanec.h"
#include "Reditel.h"
using namespace std;

int main()
{
    Reditel* karel = new Reditel("Karel Kos");
    Zamestnanec* pepa = new Zamestnanec("Pepa Nos", 10000);
    Zamestnanec* jan = new Zamestnanec("Jan Nos", 10000);

    cout << "Jmeno asistenta: " << karel->getJmenoAsistenta() << endl;
    karel->zmenAsistenta(pepa);
    cout << "Jmeno asistenta: " << karel->getJmenoAsistenta() << endl;
    karel->zmenAsistenta(jan);
    cout << "Jmeno asistenta: " << karel->getJmenoAsistenta() << endl;
    karel->zvysPlatAsistenta(1000);
    cout << "Plat Jana: " << jan->getPlat() << endl;
    delete jan;
    delete pepa;
    delete karel;
    return 0;
}
