#include "Zamestnanec.h"

Zamestnanec::Zamestnanec(string jmeno, int plat) {
	m_jmeno = jmeno;
	m_plat = plat;
}

string Zamestnanec::getJmeno() {
	return m_jmeno;
}

int Zamestnanec::getPlat() {
	return m_plat;
}

void Zamestnanec::zvysPlat(int zvyseni) {
    m_plat += zvyseni;
}
