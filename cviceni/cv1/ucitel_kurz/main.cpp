#include <iostream>
using namespace std;

class Ucitel{
public:
    string m_krestniJmeno;
    string m_prijemni;
    string m_ustav;
    int m_vek;

    void printInfo(){
        cout << "Jmeno: " << m_krestniJmeno << endl;
        cout << "Prijmeni: " << m_prijemni << endl;
    }
};

class Kurz{
public:
    string m_nazev;
    int m_maxPocetStudentu;

    void printInfo(){
        cout << "Nazev: " << m_nazev << endl;
        cout << "Max. pocet: " << m_maxPocetStudentu << endl;
    }
};


int main(){
    Ucitel* david = new Ucitel;
    Ucitel* karel = new Ucitel;

    david->m_krestniJmeno = "David";
    david->m_prijemni = "Prochazka";
    david->m_vek = 35;
    karel->m_krestniJmeno = "Karel";
    karel->m_prijemni = "Zidek";
    david->m_vek = 25;

    Kurz* zoo = new Kurz;
    zoo->m_nazev = "ZOO";
    zoo->m_maxPocetStudentu = 190;

    david->printInfo();
    karel->printInfo();
    zoo->printInfo();

    delete zoo;
    delete david;
    delete karel;
    return 0;
}
