#ifndef BYTOST_H
#define BYTOST_H

#include <iostream>

class Bytost {

protected:
	std::string m_jmeno;
	int m_sila;
	int m_obratnost;
	int m_zivoty;
	int m_magie;

public:
	Bytost(std::string jmeno, int sila, int obratnost, int magie);
	virtual void zvysUroven();
	virtual void printInfo();
	void zautoc(Bytost* nepritel);
	int getUtok();
	void odectiZraneni(int zraneni);
	void zautocMagii(Bytost* nepritel);
	int getMagie();
};

#endif // BYTOST_H
