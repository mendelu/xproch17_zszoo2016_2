#include "Elf.h"

Elf::Elf(std::string jmeno, int sila, int obratnost, int magie):
    Bytost(jmeno, sila, obratnost, magie) {

}

void Elf::printInfo() {
	std::cout << "Bytost Elf: " << std::endl;
	Bytost::printInfo();
}

void Elf::zvysUroven() {
	/// dokud je sila > 5, odecte se mu
	if (m_sila > 5){
        m_sila--;
	}
	m_obratnost += 2;
	m_magie += 2;
}
