#include "Bytost.h"

Bytost::Bytost(std::string jmeno, int sila, int obratnost, int magie) {
	m_jmeno = jmeno;
	m_sila = sila;
	m_obratnost = obratnost;
	m_magie = magie;
	m_zivoty = 100;
}

void Bytost::zvysUroven() {
	m_sila++;
	m_obratnost++;
	m_magie++;
}

void Bytost::printInfo() {
	std::cout << "Hodnoty postavy " << m_jmeno << ": " << std::endl;
	std::cout << "- Sila " << m_sila << std::endl;
	std::cout << "- Obratnost " << m_obratnost << std::endl;
	std::cout << "- Magie " << m_magie << std::endl;
	std::cout << "- Zivoty " << m_zivoty << std::endl << std::endl;
}

void Bytost::zautoc(Bytost* nepritel) {
	/// porovnam svuj utok a utok nepritele
    if (getUtok() > nepritel->getUtok()){
        /// pokud budu mit vetsi utok ja, odectu mu zraneni
        nepritel->odectiZraneni(getUtok()-nepritel->getUtok());
    } else {
        /// pokud bude mit vetsi utok on, zraneni dostanu ja
        odectiZraneni(nepritel->getUtok()-getUtok());
    }
}

int Bytost::getUtok() {
	return m_sila*m_obratnost;
}

void Bytost::odectiZraneni(int zraneni) {
    if (zraneni > m_zivoty){
        /// pokud bude muj zivot mensi nez zraneni, tak zemru
        m_zivoty = 0;
        std::cout << "Bytost " << m_jmeno << " zemrela" << std::endl;
    } else {
        /// pokud ne, tak se normalne odecte
        m_zivoty -= zraneni;
    }
}

void Bytost::zautocMagii(Bytost* nepritel) {
	/// porovnam svuj utok a utok nepritele
    if (m_magie > nepritel->getMagie()){
        /// pokud budu mit vetsi utok ja, odectu mu zraneni
        nepritel->odectiZraneni(m_magie-nepritel->getMagie());
    } else {
        /// pokud bude mit vetsi utok on, zraneni dostanu ja
        odectiZraneni(nepritel->getMagie()-m_magie);
    }
}

int Bytost::getMagie() {
	return m_magie;
}
