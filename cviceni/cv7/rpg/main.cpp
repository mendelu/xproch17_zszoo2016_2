#include <iostream>
#include "Clovek.h"
#include "Elf.h"
using namespace std;

int main()
{
    Bytost* nepritel1 = nullptr;
    Bytost* nepritel2 = nullptr;

    std::cout << "Vyber bytost ([c]lovek, [e]lf): ";
    std::string rozhodnuti = "";
    std::cin >> rozhodnuti;

    if (rozhodnuti == "c"){
        nepritel1 = new Clovek("Karel", 10, 10);
    } else {
        nepritel1 = new Elf("Legolas", 5, 12, 15);
    }

    std::cout << "Vyber bytost ([c]lovek, [e]lf): ";
    std::cin >> rozhodnuti;

    if (rozhodnuti == "c"){
        nepritel2 = new Clovek("Pepa", 10, 10);
    } else {
        nepritel2 = new Elf("Lego", 5, 12, 15);
    }

    nepritel1->printInfo();
    nepritel2->printInfo();
    nepritel1->zautoc(nepritel2);
    nepritel1->zvysUroven();
    nepritel1->printInfo();
    nepritel2->printInfo();

    delete nepritel1;
    delete nepritel2;
    return 0;
}
