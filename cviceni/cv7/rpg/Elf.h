#ifndef ELF_H
#define ELF_H

#include <iostream>
#include "Bytost.h"

class Elf : public Bytost {
public:
	Elf(std::string jmeno, int sila, int obratnost, int magie);
	void printInfo();
	void zvysUroven();
};

#endif // ELF_H
