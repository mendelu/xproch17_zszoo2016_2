#include "Clovek.h"

Clovek::Clovek(std::string jmeno, int sila, int obratnost):
    Bytost(jmeno, sila, obratnost, 0) {

}

void Clovek::printInfo() {
	std::cout << "Bytost Clovek: " << std::endl;
	Bytost::printInfo();
}

void Clovek::zvysUroven() {
	m_sila++;
	m_obratnost += 2;
	m_magie++;
}
