#include <iostream>
using namespace std;

class Balik{
public:
    string m_prijemce;
    string m_odesilatel;
    float m_vaha;
    int m_dobirka;

    Balik(string prijemce, string odesilatel, float vaha){
        m_prijemce = prijemce;
        m_odesilatel = odesilatel;
        m_dobirka = 0;
        //m_vaha = vaha;
        setVaha(vaha);
    }

    Balik(string prijemce, string odesilatel, float vaha, int dobirka){
        m_prijemce = prijemce;
        m_odesilatel = odesilatel;
        m_dobirka = dobirka;
        //m_vaha = vaha;
        setVaha(vaha);
    }

    Balik(string prijemce, string odesilatel){
        m_prijemce = prijemce;
        m_odesilatel = odesilatel;
        m_dobirka = 0;
        m_vaha = 0.0f;
    }

    Balik(string prijemce, string odesilatel, int dobirka){
        m_prijemce = prijemce;
        m_odesilatel = odesilatel;
        m_dobirka = dobirka;
        m_vaha = 0.0f;
    }

	// kontroluji, zda nekdo nedava podivnou vahu
    void setVaha(float vaha){
        if (vaha < 0){
            m_vaha = 0;
        } else {
            m_vaha = vaha;
        }
    }

    void printInformace(){
        cout << "\n------- Balik -------\n";
        cout << "Prijemce:" << m_prijemce << endl;
        cout << "Odesilatel:" << m_odesilatel << endl;
        cout << "Vaha:" << m_vaha << endl;
        cout << "Dobirka:" << m_dobirka << endl;
    }
};

class Posta{
public:
    void vyberPracovnika(){
        string typPracovnika = "";
        cout << "Zadej typ [p]obocka nebo [t]eren: ";
        cin >> typPracovnika;

        if (typPracovnika == "p"){
            praceNaPobocce();
        } else {
            praceVTerenu();
        }
    }

    // Nasledujici dve metody jsou zcela spatne!
    // Obsahuji prakticky totozny kod.
    // Je potreba se zamystet, jak je rozdelit,
    // aby se kod neduplikoval.
    void praceVTerenu(){
        string odesilatel = "";
        cout << "Zadej jmeno odesilatele: ";
        cin >> odesilatel;

        string prijemce = "";
        cout << "Zadej jmeno prijemce: ";
        cin >> prijemce;

        string budeDobirka = "";
        cout << "Bude dobirka [a/n]: ";
        cin >> budeDobirka;

        Balik* mujBalik = nullptr; // neukazuji na zadny objekt
        if (budeDobirka == "a"){
            int dobirka = 0;
            cout << "Zadej dobirku: ";
            cin >> dobirka;

            mujBalik = new Balik(prijemce, odesilatel, dobirka);
        } else {
            mujBalik = new Balik(prijemce, odesilatel);
        }
        mujBalik->printInformace();

        delete mujBalik;

    }

    void praceNaPobocce(){
        string odesilatel = "";
        cout << "Zadej jmeno odesilatele: ";
        cin >> odesilatel;

        string prijemce = "";
        cout << "Zadej jmeno prijemce: ";
        cin >> prijemce;

        float vaha = 0.0f;
        cout << "Zadej vahu: ";
        cin >> vaha;

        string budeDobirka = "";
        cout << "Bude dobirka [a/n]: ";
        cin >> budeDobirka;

        Balik* mujBalik = nullptr; // neukazuji na zadny objekt
        if (budeDobirka == "a"){
            int dobirka = 0;
            cout << "Zadej dobirku: ";
            cin >> dobirka;

            mujBalik = new Balik(prijemce, odesilatel, vaha, dobirka);
        } else {
            mujBalik = new Balik(prijemce, odesilatel, vaha);
        }
        mujBalik->printInformace();

        delete mujBalik;
    }
};


int main()
{
    // Hlavni funkce programu by nikdy nemela obsahovat
    // velke mnozstvi kodu. Pouze vytvoreni zvolenych
    // inicializacnich objektu a zavolani "startovacich"
    // metod.
    Posta* mojePosta = new Posta;
    mojePosta->vyberPracovnika();
    delete mojePosta;
    return 0;
}