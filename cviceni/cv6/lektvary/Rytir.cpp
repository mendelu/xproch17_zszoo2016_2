#include "Rytir.h"

Rytir::Rytir(int sila, int obrana, int bonusObranyBrneni) {
	m_sila = sila;
	m_obrana = obrana;
	m_zivot = 100;
	m_neseneBrneni = new Brneni(bonusObranyBrneni);
}

Rytir::~Rytir() {
    delete m_neseneBrneni;
}

void Rytir::printInfo() {
	std::cout << "Sila: " << m_sila << std::endl;
	std::cout << "Zivot: " << m_zivot << std::endl;
}

void Rytir::seberLektvar(Lektvar* nalezenyLektvar) {
	if(m_inventar.size() < 10){
        m_inventar.push_back(nalezenyLektvar);
	} else {
        std::cout << "jsem plnej" << std::endl;
	}
}

void Rytir::vypijLektvar() {
	/// hodnotu bbonusu sily a zdravi u posledniho lektvaru
	/// prictu ke sve sile a zivotu
	int indexPoslednihoNapoje = m_inventar.size()-1;
	Lektvar* posledniLektvar = m_inventar.at(indexPoslednihoNapoje);
    m_sila += posledniLektvar->getBonusSily();
    m_zivot += posledniLektvar->getBonusZivota();

	/// vypity lektvar smazu
    m_inventar.pop_back();
}
