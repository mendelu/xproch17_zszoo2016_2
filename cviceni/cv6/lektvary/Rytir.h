#ifndef RYTIR_H
#define RYTIR_H

#include <vector>
#include <iostream>
#include "Brneni.h"
#include "Lektvar.h"

class Rytir {

private:
	int m_zivot;
	int m_sila;
	int m_obrana;
	Brneni* m_neseneBrneni;
	std::vector<Lektvar*> m_inventar;

public:
	Rytir(int sila, int obrana, int bonusObranyBrneni);
	~Rytir();
	void printInfo();
	void seberLektvar(Lektvar* nalezenyLektvar);
	void vypijLektvar();
};

#endif // RYTIR_H
