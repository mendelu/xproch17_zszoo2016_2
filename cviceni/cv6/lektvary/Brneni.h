#ifndef BRNENI_H
#define BRNENI_H

class Brneni {

private:
	int m_bonusObrany;

public:
	Brneni(int bonusObrany);
	int getBonusObrany();
};

#endif // BRNENI_H
