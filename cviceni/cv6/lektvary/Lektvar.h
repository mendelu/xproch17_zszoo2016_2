#ifndef LEKTVAR_H
#define LEKTVAR_H

class Lektvar {

private:
	int m_bonusSily;
	int m_bonusZivota;

public:
	Lektvar(int bonusSily, int bonusZivota);
	int getBonusSily();
	int getBonusZivota();
};

#endif
