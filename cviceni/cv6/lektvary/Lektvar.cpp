#include "Lektvar.h"

Lektvar::Lektvar(int bonusSily, int bonusZivota) {
	m_bonusSily = bonusSily;
	m_bonusZivota = bonusZivota;
}

int Lektvar::getBonusSily() {
	return m_bonusSily;
}

int Lektvar::getBonusZivota() {
	return m_bonusZivota;
}
