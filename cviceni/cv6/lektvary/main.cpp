#include <iostream>
#include "Rytir.h"
#include "Lektvar.h"
using namespace std;

int main()
{
    Rytir* david = new Rytir(100, 100, 50);
    Lektvar* redBull = new Lektvar(50, -10);
    david->seberLektvar(redBull);
    david->printInfo();

    david->vypijLektvar();
    david->printInfo();

    delete redBull;
    delete david;
    return 0;
}
