#include "Zakazka.h"

Zakazka::Zakazka(std::string popis, int naklady, Zakaznik* objednavatel) {
	m_popis = popis;
	m_naklady = naklady;
	m_objednavatel = objednavatel;
}

Zakazka::Zakazka(std::string popis, int naklady, int idObjednavatele) {
	m_popis = popis;
	m_naklady = naklady;
	m_objednavatel = Spravce::getZakaznikById(idObjednavatele);
}

int Zakazka::getFakturovanaCena() {
	return round(m_naklady*1.5);
}

void Zakazka::printInfo() {
	std::cout << "Fakturovano: " << getFakturovanaCena() << std::endl;
	std::cout << "Popis: " << m_popis << std::endl;
	std::cout << "Objednal: " << m_objednavatel->getJmeno() << std::endl;
}
