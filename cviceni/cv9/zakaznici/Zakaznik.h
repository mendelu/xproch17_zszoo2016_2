#ifndef ZAKAZNIK_H
#define ZAKAZNIK_H

#include <iostream>

class Zakaznik {
	int m_id;
	std::string m_jmeno;

public:
	Zakaznik(int id, std::string jmeno);
	int getId();
	std::string getJmeno();
};

#endif // ZAKAZNIK_H
