#include <iostream>
#include "Zakaznik.h"
#include "Zakazka.h"
#include "Spravce.h"
using namespace std;

int main()
{
    Zakaznik* pepa = Spravce::createZakaznik("Pepa Nos");
    int idPepika = pepa->getId();
    Zakazka* oprava = new Zakazka("oprava vody", 1000, idPepika);
    oprava->printInfo();

    return 0;
}
