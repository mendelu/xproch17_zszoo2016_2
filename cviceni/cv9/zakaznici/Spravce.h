#ifndef SPRAVCE_H
#define SPRAVCE_H

#include <vector>
#include <iostream>
#include "Zakaznik.h"

class Spravce {
	static std::vector<Zakaznik*> s_zakaznici;
	static int s_pocetZakazniku;
public:
	static Zakaznik* getZakaznikById(int id);
	static std::vector<Zakaznik*> getZakaznikByJmeno(std::string jmeno);
	static Zakaznik* createZakaznik(std::string jmeno);
	static void printAllCustomers();
private:
	Spravce();
};
#endif // SPRAVCE_H
