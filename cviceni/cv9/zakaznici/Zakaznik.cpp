#include "Zakaznik.h"

Zakaznik::Zakaznik(int id, std::string jmeno) {
	m_id = id;
	m_jmeno = jmeno;
}

int Zakaznik::getId() {
	return m_id;
}

std::string Zakaznik::getJmeno() {
	return m_jmeno;
}
