#ifndef ZAKAZKA_H
#define ZAKAZKA_H

#include <iostream>
#include <cmath>
#include "Zakaznik.h"
#include "Spravce.h"

class Zakazka {
	std::string m_popis;
	int m_naklady;
	Zakaznik* m_objednavatel;

public:
	Zakazka(std::string popis, int naklady, Zakaznik* objednavatel);
	Zakazka(std::string popis, int naklady, int idObjednavatele);
	int getFakturovanaCena();
	void printInfo();
};


#endif // ZAKAZKA_H
